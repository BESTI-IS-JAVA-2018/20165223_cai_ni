#include "Main.h"
#include "BootApi.h"
#include "Gpio.h"
#include "Uart.h"

UINT8 shuju_lens;
UINT8 shuju[64];
UINT8 uart_rx_num;
UINT8 uart_rx_end;

int main(void)
{
    /*********************此段代码勿动***********************/
    //系统中断向量设置，使能所有中断
    SystemInit ();
    //返回boot条件
    if(0 == GPIO_GetVal(0))
    {
        BtApiBack(0x55555555, 0xAAAAAAAA);
    }
    /*********************此段代码勿动***********************/
    UART_Init();    //初始化Uart
    UART_SendByte('A');                                           //Uart发送一个字符A
    UART_SendByte('\r');UART_SendByte('\n');        //换行
    UART_SendString("Welcome to Z32HUA!");       //Uart发送字符串
    UART_SendByte('\r');UART_SendByte('\n');        //换行
    UART_SendNum(1234567890);                          //Uart发送一个十进制数
    UART_SendByte('\r');UART_SendByte('\n');        //换行
    UART_SendHex(0xAA);                                       //Uart发送一个十六进制数
    UART_SendByte('\r');UART_SendByte('\n');        //换行
    while(1)
    {
        if(uart_rx_end)
        {
            uart_rx_end=0;
            uart_SendString(shuju,shuju_lens);
        }
    }        //等待接收中断。
}

//延时函数，当系统时钟为内部 OSC 时钟时，延时 1ms
void delay(int ms)
{
    int i;
    while(ms--)
    {
    for(i=0;i<950;i++) ;
    }
}