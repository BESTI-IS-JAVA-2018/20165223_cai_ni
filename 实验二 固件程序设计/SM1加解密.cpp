#include "Main.h"
#include "BootApi.h"
#include "Gpio.h"
#include "Uart.h"
#include "LCD.h"
#include "KEY.h"
#include "SLE4428.h"
#include "SM1.h"

UINT8 jiamiqian[16]={0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F};
UINT8 jiamimiyue[16]={0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F};
UINT8 jiamihou[16];
UINT8 jiemiqian[16],jiemimiyue[16],jiemihou[16];
UINT8 cuowumiyue[16]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
UINT8 UserCode[5];
UINT8 C;

int main(void)
{
    /*********************此段代码勿动***********************/
    //系统中断向量设置，使能所有中断
    SystemInit ();
    //返回boot条件
    if(0 == GPIO_GetVal(0))
    {
        BtApiBack(0x55555555, 0xAAAAAAAA);
    }
    /*********************此段代码勿动***********************/    
    /*初始化IC卡插入检测IO口GPIO6*/
    GPIO_Config(6);     
    GPIO_PuPdSel(6,0);  //上拉
    GPIO_InOutSet(6,1); //输入       
    UART_Init();
    lcd_init();
    KEY_Init();
    lcd_pos(0,0);//定位第一行
    lcd_string("SLE4428 实验！");
A:  while(1)
    {
        lcd_pos(1,0);//定位第二行
        lcd_string("请插入IC卡.  ");
        delay(1000);
        if(GPIO_GetVal(6)==0) break;
        
        lcd_pos(1,0);//定位第二行
        lcd_string("请插入IC卡.. ");
        delay(1000);
        if(GPIO_GetVal(6)==0) break;
        
        lcd_pos(1,0);//定位第二行
        lcd_string("请插入IC卡...");
        delay(1000);
        if(GPIO_GetVal(6)==0) break;
        
    }
    if(SLE4428_InitAndRST(2)!=0xFFFFFFFF)  //收到ATR
    {
        lcd_pos(1,0);//定位第二行
        lcd_string("已插入SLE4428");
    }
    else
    {
        lcd_pos(1,0);//定位第二行
        lcd_string("卡不正确     ");
        SLE4428_Deactivation(); //下电，去激活
        delay(1000);
        goto A; 
    }
    lcd_pos(2,0);//定位第三行
    lcd_string("用户代码为：");
    SLE4428_ReadData(0x15,UserCode,6); //读取用户代码
    lcd_pos(3,0);//定位第四行
    for(UINT8 i=0;i<6;i++)
        lcd_Hex(UserCode[i]) ;
    while(KEY_ReadValue()!='A'); //等待A键按下
    lcd_wcmd(0x01);//清屏
    lcd_pos(0,0);//定位第一行
    lcd_string("按-A键校验密码");
    lcd_pos(1,0);//定位第二行
    lcd_string("校验0xFF,0xFF");
    while(KEY_ReadValue()!='A'); //等待A键按下
    lcd_pos(2,0);//定位第三行
    if(SLE4428_PassWord(0xFF,0xFF)==1)    
        lcd_string("校验成功");
    else
        {lcd_string("校验失败"); return 0;}

    lcd_pos(3,0);//定位第四行
    switch(SLE4428_ReadByte(0x03fd))   //查看剩余密码验证机会
    {
        case 0xff: lcd_string("剩余机会： 8次");break;
        case 0x7f: lcd_string("剩余机会： 7次");break;
        case 0x3f: lcd_string("剩余机会： 6次");break;
        case 0x1f: lcd_string("剩余机会： 5次");break;
        case 0x0f: lcd_string("剩余机会： 4次");break;
        case 0x07: lcd_string("剩余机会： 3次");break;
        case 0x03: lcd_string("剩余机会： 2次");break;
        case 0x01: lcd_string("剩余机会： 1次");break;
        case 0x00: lcd_string("剩余机会： 0次");break;
        default: break;
    }
    while(KEY_ReadValue()!='A'); //等待A键按下
B:  lcd_wcmd(0x01);//清屏
    lcd_pos(0,0);//定位第一行
    lcd_string("加密解密实验");
    lcd_pos(1,0);//定位第二行
    lcd_string("1.加密");
    lcd_pos(2,0);//定位第三行
    lcd_string("2.解密");
    do
    {
        C=KEY_ReadValue();
    }
    while(C!='1'&&C!='2'); //等待1或2键按下
    lcd_wcmd(0x01);//清屏
    if(C=='1')  goto jiami;
    else if(C=='2') goto jiemi;
    else ;
jiami:
    lcd_pos(0,0);//定位第一行
    lcd_string("观看串口调试助手");
    lcd_pos(1,0);//定位第二行
    lcd_string("A 键确认加密");
    UART_SendString("将加密以下数据:\r\n");
    for(UINT8 i=0;i<16;i++)
    {
        UART_SendHex(jiamiqian[i]);
    }
    UART_SendString("\r\n");
    UART_SendString("加密密钥:\r\n");
    for(UINT8 i=0;i<16;i++)
    {
        UART_SendHex(jiamimiyue[i]);
    }
    UART_SendString("\r\n");
    while(KEY_ReadValue()!='A'); //等待A键按下

    SM1_Init(jiamimiyue);        //SM1初始化
    SM1_Crypto(jiamiqian, 16, 0, 0, 0,jiamihou); //进行加密
    SM1_Close(); //关闭安全模块
    UART_SendString("加密后的数据:\r\n");
    for(UINT8 i=0;i<16;i++)
    {
        UART_SendHex(jiamihou[i]);
    }
    UART_SendString("\r\n");
    lcd_pos(2,0);//定位第三行
    lcd_string("加密完成");
    lcd_pos(3,0);//定位第四行
    lcd_string("A 键存入IC卡");
    while(KEY_ReadValue()!='A'); //等待A键按下
    for(UINT8 i=0;i<16;i++)
    {
        SLE4428_Write_Byte(0x20+i,jiamihou[i]); //设置IC卡 0x20地址为存储加密数据的地址
    }
    UART_SendString("已将数据写入IC卡。\r\n");
    UART_SendString("\r\n");
    goto B;
jiemi:
    lcd_pos(0,0);//定位第一行
    lcd_string("观看串口调试助手");
    lcd_pos(1,0);//定位第二行
    lcd_string(" A键读取IC卡数据");
    while(KEY_ReadValue()!='A'); //等待A键按下
    SLE4428_ReadData(0x20,jiemiqian,16);
    UART_SendString("读取的数据为：\r\n");
    for(UINT8 i=0;i<16;i++)
    {
        UART_SendHex(jiemiqian[i]);
    }
    UART_SendString("\r\n");
    lcd_wcmd(0x01);//清屏
    lcd_pos(0,0);//定位第一行
    lcd_string("读取成功");
    lcd_pos(1,0);//定位第二行
    lcd_string("选择密钥解密：");
    lcd_pos(2,0);//定位第三行
    lcd_string("1.正确密钥");
    lcd_pos(3,0);//定位第四行
    lcd_string("2.错误密钥");
    do
    {
        C=KEY_ReadValue();
    }
    while(C!='1'&&C!='2'); //等待1或2键按下
    lcd_wcmd(0x01);//清屏
    if(C=='1')  
    {
    for(UINT8 i=0;i<16;i++)
        jiemimiyue[i] = jiamimiyue[i];  
    }
    else if(C=='2')
    {
    for(UINT8 i=0;i<16;i++)
        jiemimiyue[i] = cuowumiyue[i];
    }
    else ;
    UART_SendString("将使用以下密钥进行解密：\r\n");
    for(UINT8 i=0;i<16;i++)
    {
        UART_SendHex(jiemimiyue[i]);
    }
    UART_SendString("\r\n");
    lcd_pos(0,0);//定位第一行
    lcd_string("A 键确认解密");
    while(KEY_ReadValue()!='A'); //等待A键按下
    SM1_Init(jiemimiyue);        //SM1初始化
    SM1_Crypto(jiemiqian, 16, 1, 0, 0,jiemihou); //进行解密
    SM1_Close(); //关闭安全模块
    lcd_pos(1,0);//定位第二行
    lcd_string("解密完成");
    lcd_pos(2,0);//定位第三行
    lcd_string("A 键返回");
    UART_SendString("解密后的数据为：\r\n");
    for(UINT8 i=0;i<16;i++)
    {
        UART_SendHex(jiemihou[i]);
    }
    UART_SendString("\r\n");
    UART_SendString("\r\n");
    while(KEY_ReadValue()!='A'); //等待A键按下
    goto B;
    SLE4428_Deactivation(); //下电，去激活,实验结束
    while(1)
    {

    }
}
//延时函数，当系统时钟为内部OSC时钟时，延时1ms
void delay(int ms)
{
    int i;
    while(ms--)
    {
    for(i=0;i<950;i++) ;
    }
}