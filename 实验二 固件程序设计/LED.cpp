#include "Main.h"
#include "BootApi.h"
#include "Gpio.h"

int main(void)
{
    /*********************此段代码勿动***********************/
    //系统中断向量设置，使能所有中断
    SystemInit ();
    //返回boot条件
    if(0 == GPIO_GetVal(0))
    {
        BtApiBack(0x55555555, 0xAAAAAAAA);
    }
    /*********************此段代码勿动***********************/
    GPIO_PuPdSel(0,0);            //设置 GPIO0 为上拉
    GPIO_InOutSet(0,0);            //设置 GPIO0 为输出
    while(1)
    {
        delay(100);
        GPIO_SetVal(0,0);        //输出低电平，点亮 LED
        delay(100);
        GPIO_SetVal(0,1);         //输出高电平，熄灭 LED
    }
}

//延时函数，当系统时钟为内部 OSC 时钟时，延时 1ms
void delay(int ms)
{
    int i;
    while(ms--)
    {
    for(i=0;i<950;i++) ;
    }
}