#include<netinet/in.h>  // sockaddr_in  
#include<sys/types.h>   // socket  
#include<sys/socket.h>  // socket  
#include<stdio.h>       // printf  
#include<stdlib.h>      // exit  
#include<string.h>      // bzero  
#include <unistd.h>

#define SERVER_PORT 5223
#define LENGTH_OF_LISTEN_QUEUE 20  
#define BUFFER_SIZE 1024
#define FILE_NAME_MAX_SIZE 512  
#define BEGIN 1; 

int main(void)  
{  

	struct sockaddr_in server_addr;  
	bzero(&server_addr, sizeof(server_addr));  
	server_addr.sin_family = AF_INET;  
	server_addr.sin_addr.s_addr = htons(INADDR_ANY);  
	server_addr.sin_port = htons(SERVER_PORT);  

	int server_socket_fd = socket(PF_INET, SOCK_STREAM, 0);  
	if(server_socket_fd < 0)  
	{  
		perror("Create Socket Failed!");  
	    	exit(1);  
	}  
	int opt = 1;  
	setsockopt(server_socket_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));  

	if(-1 == (bind(server_socket_fd, (struct sockaddr*)&server_addr, sizeof(server_addr))))  
	{  
	    	perror("Server Bind Failed!");  
	    	exit(1);  
	}  
	    
	if(-1 == (listen(server_socket_fd, LENGTH_OF_LISTEN_QUEUE)))  
	{  
	    	perror("Server Listen Failed!");  
	    	exit(1);  
	}  

	while(1)  
	{  
	    	struct sockaddr_in client_addr;  
	    	socklen_t client_addr_length = sizeof(client_addr);  

	    	int new_server_socket_fd = accept(server_socket_fd, (struct sockaddr*)&client_addr, &client_addr_length);  
	    	if(new_server_socket_fd < 0)  
	    	{  
			perror("Server Accept Failed!");  
			break;  
	    	}  


		char buffer[BUFFER_SIZE];  
		bzero(buffer, BUFFER_SIZE);  
		if(recv(new_server_socket_fd, buffer, BUFFER_SIZE, 0) < 0)  
		{  
			perror("Server Recieve Data Failed!");  
			break;  
		}  


	    	char file_name[FILE_NAME_MAX_SIZE+1];  
	    	bzero(file_name, FILE_NAME_MAX_SIZE+1);  
	    	strncpy(file_name, buffer, strlen(buffer)>FILE_NAME_MAX_SIZE?FILE_NAME_MAX_SIZE:strlen(buffer));  
		printf("%s\n", file_name);  

	    	FILE *fp = fopen(file_name, "w");  
	    	if(NULL == fp)  
	    	{  
			printf("File: %s Can Not Open To Write\n", file_name);  
			exit(1);  
	    	}  


	    	bzero(buffer, BUFFER_SIZE);  
	    	int length = 0;  
	    	while((length = recv(new_server_socket_fd, buffer, BUFFER_SIZE, 0)) > 0)  
	    	{  
		    	if(strcmp(buffer,"OK")==0) 
				break;
			if(fwrite(buffer, sizeof(char), length, fp) < length)  
			{  
			    	printf("File: %s Write Failed!\n", file_name);  
			    	break;  
			}  
			bzero(buffer, BUFFER_SIZE);  
		}  

	    	printf("Receive File: %s From Client Successful!\n", file_name);  
	    	fclose(fp);

		int words=0;
		char s[100];
		FILE *fp2;
		if((fp2=fopen(file_name,"r"))==NULL)
		{
	    		printf("ERROR!\n");
	    		exit(0);
		}
		while(fscanf(fp2,"%s",s)!=EOF)
	    	words++;
		fclose(fp2);
		printf("%d words.\n",words);

		char sendbuf[50];
		sprintf(sendbuf,"%d",words);
		send(new_server_socket_fd,sendbuf,50,0);
	    	close(new_server_socket_fd);  
	}
	close(server_socket_fd);  
	return 0;  
}
