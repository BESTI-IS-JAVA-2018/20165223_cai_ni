#include<netinet/in.h>   // sockaddr_in  
#include<sys/types.h>    // socket  
#include<sys/socket.h>   // socket  
#include<stdio.h>        // printf  
#include<stdlib.h>       // exit  
#include<string.h>       // bzero  
#include <arpa/inet.h>
#include <unistd.h>
#include "head.h"

#define SERVER_PORT 5223
#define BUFFER_SIZE 1024  
#define FILE_NAME_MAX_SIZE 512  
#define BEGIN 1;

int main()  
{
	struct sockaddr_in client_addr;  
	bzero(&client_addr, sizeof(client_addr));  
	client_addr.sin_family = AF_INET;  
	client_addr.sin_addr.s_addr = htons(INADDR_ANY);  
	client_addr.sin_port = htons(0);  
	int client_socket_fd = socket(AF_INET, SOCK_STREAM, 0);  
	if(client_socket_fd < 0)  
	{  
    		perror("Create Socket Failed!");  
    		exit(1);  
	}  
	if(-1 == (bind(client_socket_fd, (struct sockaddr*)&client_addr, sizeof(client_addr))))  
	{  
    		perror("Client Bind Failed!");  
    		exit(1);  
	}
	struct sockaddr_in server_addr;  
	bzero(&server_addr, sizeof(server_addr));  
	server_addr.sin_family = AF_INET;  
	if(inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr) == 0)  
	{  
    		perror("Server IP Address Error!");  
    		exit(1);  
	}  
	server_addr.sin_port = htons(SERVER_PORT);  
	socklen_t server_addr_length = sizeof(server_addr);  
	if(connect(client_socket_fd, (struct sockaddr*)&server_addr, server_addr_length) < 0)  
	{  
    		perror("Can Not Connect To Server IP!");  
    		exit(0);  
	}  
	char file_name[FILE_NAME_MAX_SIZE+1];  
	bzero(file_name, FILE_NAME_MAX_SIZE+1);  
	printf("Please Input File Name On Client: ");
	scanf("%s", file_name);  
	char buffer[BUFFER_SIZE];  
	bzero(buffer, BUFFER_SIZE);  
	strncpy(buffer, file_name, strlen(file_name)>BUFFER_SIZE?BUFFER_SIZE:strlen(file_name));  
	if(send(client_socket_fd, buffer, BUFFER_SIZE, 0) < 0)  
	{  
    		perror("Send File Name Failed!");  
    		exit(1);  
	}
    	FILE *fp = fopen(file_name, "r");  
    	if(NULL == fp)  
    	{  
        	printf("File: %s Not Found!\n", file_name);  
    	}  
    	else  
    	{  
        	bzero(buffer, BUFFER_SIZE);  
        	int length = 0;  
        	while((length = fread(buffer, sizeof(char), BUFFER_SIZE, fp)) > 0)
        	{
            		if(send(client_socket_fd, buffer, length, 0) < 0)  
            		{  
                		printf("Send File: %s Failed!/n", file_name);  
                		break;  
            		}  
            		bzero(buffer, BUFFER_SIZE);  
        	}  
		printf("Send File: %s Successful!\n", file_name);
		printf("The File has %d words.\n",wcfunc(file_name));	  
    	}  
/*
	char s[50];
	scanf("%s",s);
	send(client_socket_fd,"OK",50,0);

	char recvdata[sizeof(int)+1];
	recv(client_socket_fd,recvdata,sizeof(int),0);
	recvdata[sizeof(int)]='\0';
	int words=atoi(recvdata);*/
	fclose(fp);
	close(client_socket_fd);  
	return 0;  
}

int wcfunc(char *file_name)
{
	int t;
	int w = 0;
	int state = 0;
	FILE *in;
	if((in = fopen(file_name,"r"))==NULL)
	{
		printf("wc %s:no this file or dir\n",file_name);
		return 0;
	}
	while((t=fgetc(in))!=EOF)
	{
		
		if(t=='\n'||t==' '||t=='\r') {
            		state = 0;
            		continue;
        	} else {
            		if(state == 0) {
                	state = 1;
                	w++;
           		}
            		continue;
        	}
	}
	return w;
}
