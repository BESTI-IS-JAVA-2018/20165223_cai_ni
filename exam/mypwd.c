#include<stdio.h>  
#include<sys/stat.h>  
#include<dirent.h>  
#include<stdlib.h>  
#include<string.h>  
#include<sys/types.h> 
#include <unistd.h> 
void printpath();  
char *inode_to_name(int);  
int getinode(char *);  
//功能：打印当前目录路径
void printpath()  
{  
    int inode,up_inode;  
    char *str;
    inode = getinode(".");  
    up_inode = getinode("..");  
    chdir("..");  
    str = inode_to_name(inode);  
    //当当前目录的i-node与父级目录的i-node相同时，到达根目录
    if(inode == up_inode) {  
        return;  
    }  
    //打印路径
    printpath();  
    printf("/%s",str);  
}  
//功能：获取当前目录的i-node
int getinode(char *str)  
{  
    struct stat st;  
    if(stat(str,&st) == -1){  
        perror(str);  
        exit(-1);  
    }  
    return st.st_ino;  
}  
//功能：获取当前路径
char *inode_to_name(int inode)  
{  
    char *str;  
    DIR *dirp;  
    struct dirent *dirt;  
    if((dirp = opendir(".")) == NULL){  
        perror(".");  
        exit(-1);  
    }  
    while((dirt = readdir(dirp)) != NULL)  
    {  
        if(dirt->d_ino == inode){  
            str = (char *)malloc(strlen(dirt->d_name)*sizeof(char));  
            strcpy(str,dirt->d_name);  
            return str;  
        }  
    }  
    perror(".");  
    exit(-1);  
}  
//主函数
int main()  
{  
    printpath();  
    putchar('\n');  
    return 0;  
}