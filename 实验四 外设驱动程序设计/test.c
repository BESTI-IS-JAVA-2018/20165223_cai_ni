#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#define TEST_DEVICE_FILENAME "/dev/test_dev" /* 设备文件名*/
#define BUFF_SZ 1024 /* 缓冲大小 */
int main()
{
	int fd, nwrite, nread;
	char buff[BUFF_SZ]; /*缓冲区*/
	/* 打开设备文件 */
	fd = open(TEST_DEVICE_FILENAME, O_RDWR);
	if (fd < 0)
	{
		perror("open");
		exit(1);
	}
	do
	{
		printf("Input some words to kernel:");
		memset(buff, 0, BUFF_SZ);
		if (fgets(buff, BUFF_SZ, stdin) == NULL)
		{
			perror("fgets");
			break;
		}
		buff[strlen(buff) - 1] = '\0';
		if (write(fd, buff, strlen(buff)) < 0) /* 向设备写入数据 */
		{
			perror("write");
			break;
		}
		if (read(fd, buff, BUFF_SZ) < 0) /* 从设备读取数据 */
		{
			perror("read");
			break;
		}
		else
		{
			printf("The read string is from kernel:%s\n", buff);
		}
	}while(strncmp(buff, "quit", 4));
	close(fd);
	exit(0);
}
